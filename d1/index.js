//Comparison operator

// $eq
//$gt
//$gte
//$in
//$lt
//$lte
// $ne
//$nin ==none of the values specified in an array

//Logical operators
// db.users.insertOne(
// 	{
// 		"firstName":"Bill",
// 		"lastName":"Gates",
// 		"age":65,
// 		"contact":{
// 				"phone": "12345678",
// 				"email":"bill@email.com"
// 				},
// 		"courses": ["PHP","Laravel","HTML"],
// 		"department": "operations"
// 	}
// );

db.users.find({"age": {$lt:50}})
db.users.find({"age": {$gt:50}})
db.users.find({"age": {$gte:50}})

//age not equal to 82
db.users.find({"age": {$ne:82}})



db.users.find( { $or: [ { "lastName": { $eq: "Hawking" } }, { "lastName": {$eq: "Doe" } } ] } )


// $in == equals any value in the specified array
db.users.find(
	{
		"lastName":{$in: ["Hawking","Doe"]} 
	}
);

//documents with courses html and react
db.users.find(
	{
		"courses":{$in: ["HTML","React"]} 
	}
);

//OR
//look for --name "Jane" or age 25
db.users.find( { $or: [ { "firstName": { $eq: "Jane" } }, { "age": {$eq: 25 } } ] } )

db.users.find( {$or: [ { "firstName": "Jane" }, {"age": 25 }]})

// name Jane has age of greater than 30

db.users.find( {$or: [ { "firstName": "Jane" }, {"age": {$gt: 30}}]})

//AND
//age not equal to 82 and age not equal to 76

db.users.find( {$and: [ { "age": {$ne:82}}, {"age": {$ne: 76}}]})

//$regex --> search case insensitivity, $i --->case insensitivity

//Field Projection
//--allsows to include or exclude sp fields in returning doc

db.users.find(
	{
		"firstName":"Jane"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact": 1,
	}
);


//document that has a name Hawking, exclude contact and department fields
db.users.find(
	{
		"lastName":"Hawking"
	},
	{
		"department": 0,
		"contact": 0
	}
);

//doc that has a name Jane and exclude ID but include first and last name, contact
db.users.find(
	{
		"firstName":"Jane"
	},
	{
		"_id": 0,
		"firstName": 1,
		"lastName": 1,
		"contact": 1
		"_id": 0
	}
);

//has name Bill, include first and last name and phone number

db.users.find(
	{
		"firstName":"Bill"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact.phone": 1,
		"_id":0
	}
);

//name Bill, include everything else except email
db.users.find(
	{
		"firstName":"Bill"
	},
	{
		
		"contact.email": 0,
		
	}
);

//EVALUATION QUERY OPERATOR
//$regex operator
db.users.find(
{
	"firstName": {$regex:"N", $options:"i"}
}
);