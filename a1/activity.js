//Find users with letter s in first name or d in last name
// $or operator
//Show only firstName and lastName and hide _id

db.users.find(
	{$or: [ {"firstName": {$regex:"s", $options:"i"}}, {"lastName": {$regex:"d", $options:"i"}}]},
	{
		"firstName":1,
		"lastName":1,
		"_id":0

	}
	);

//Find users who are from HR department and age is greater than or equal to 70
//Use $and operator

db.users.find({$and: [ {"department": "HR"}, {"age": {$gte: 70}}]});



//Find users with the letter e in their first name and has an age of less than or equal to 30

db.users.find({$and: [ {"firstName": {$regex:"e", $options:"i"}}, {"age": {$lte:30}}]});
